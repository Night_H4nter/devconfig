----Maximizer config



--disable default mappings
vim.g.maximizer_set_default_mapping = 0



--automatically restore sizes when leaving maximized window
vim.g.maximizer_restore_on_winleave = 1


--toggle maximize window
vim.api.nvim_set_keymap( 'n', '<Bslash>um', ':call MaximizerSmartToggle()<cr>',
    { noremap = true, silent = true })


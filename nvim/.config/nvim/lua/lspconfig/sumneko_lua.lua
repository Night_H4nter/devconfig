--Sumneko lua LSP



--set dirs
local sumneko_root_path = os.getenv("HOME") .. 'repos/lua-language-server/'
local sumneko_binary = '/bedrock/cross/bin/lua-language-server'


--ls util and configs
-- require('lsp.util')


local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")


require'lspconfig'.sumneko_lua.setup {
    require'coq'.lsp_ensure_capabilities
        {
            cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"};
            flags = {
                debounce_text_changes = 150,
            },
            -- on_attach = function () _G.lcattached = true end,
            -- on_attach = require('intellisense.key.keys'),
            init_options = { documentFormatting = true, codeAction = true },
            settings = {
                Lua = {
                    runtime = {
                        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                        version = 'LuaJIT',
                        -- Setup your lua path
                        path = runtime_path,
                    },
                    diagnostics = {
                        -- Get the language server to recognize the `vim` global
                        globals = {'vim'},
                    },
                    workspace = {
                        -- Make the server aware of Neovim runtime files
                        library = vim.api.nvim_get_runtime_file("", true),
                        preloadFileSize = 1000,
                        maxPreload = 2000,
                    },
                    -- Do not send telemetry data containing a randomized but unique identifier
                    telemetry = {
                        enable = false,
                    },
                },
            },
        }
}


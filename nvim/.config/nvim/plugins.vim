""manage plugins with vim-plug, since both dein and packer break
call plug#begin('~/.local/share/nvim/plugins/')




""Primary UI
"treesitter
Plug 'nvim-treesitter/nvim-treesitter'
", { 'do' : ':TSInstall rust lua yaml bash json comment haskell commonlisp' }
"my colorscheme
Plug 'https://gitlab.com/Night_H4nter/ts-neosolarized-extended.git'
"just to have it for gnome and other bs that doesn't look decent with
"solarized (basically fore everything lmao)
Plug 'tomasiser/vim-code-dark'
"devicons for pretty incons
" Plug 'ryanoasis/vim-devicons'
Plug 'kyazdani42/nvim-web-devicons'
"statusline
Plug 'itchyny/lightline.vim'
"tabline
Plug 'mengelbrecht/lightline-bufferline'
"git stuff
Plug 'mhinz/vim-signify'
"startify
Plug 'mhinz/vim-startify'
"workman
Plug 'nicwest/vim-workman'
"whichkey
Plug 'folke/which-key.nvim'
"needed for comparsion
" Plug 'NightH4nter/NeoSolarized'
"export status line
Plug 'vimpostor/vim-tpipeline'




""Secondary UI
"telescope and it's dependencies
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
"better fzf
Plug 'nvim-telescope/telescope-fzy-native.nvim'
"filetree and git integration for it
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
"hints for registers
Plug 'junegunn/vim-peekaboo'
"undotree
Plug 'mbbill/undotree'
"tagbar
Plug 'liuchengxu/vista.vim'
"indent hints
Plug 'Yggdroot/indentline'
"multiple cursors because why not TODO:do i wanna use it?
Plug 'mg979/vim-visual-multi'
"highlight current word
Plug 'dominikduda/vim_current_word'
"window maximizer
Plug 'szw/vim-maximizer'




""Common plugins
"quick comments
Plug 'tpope/vim-commentary'
"add, change and remove surrounding things like parentheses
Plug 'tpope/vim-surround'
"more text objects
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-entire'
"expand selection regions
Plug 'terryma/vim-expand-region'
"edit text in browser just like in nvim
"    "requires firenvim browser extension
" Plug 'glacambre/firenvim' "TODO
"make motion easy
Plug 'easymotion/vim-easymotion'
"don't highlight when done searching
Plug 'romainl/vim-cool'
"automatically finish structures life if-else, function-endfunction, etc.
Plug 'tpope/vim-endwise'
"vim-closer for autoclosing parentheses, quotes, brackets, etc
Plug 'rstacruz/vim-closer'
"submodes TODO
" Plug 'tomtom/tlib_vim'
" Plug 'tomtom/tinykeymap_vim'
"colors previews
"Plug 'rrethy/vim-hexokinase', { 'do' : 'make hexokinase' }
"some gnu commands for help
Plug 'tpope/vim-eunuch'



""Intellisense
"best lsp implementation so far for gotos and renames
Plug 'prabirshrestha/vim-lsp'
"lsp settings
Plug 'mattn/vim-lsp-settings'
"snippet library
" Plug 'honza/vim-snippets'
"snippet integration
" Plug 'thomasfaingnaert/vim-lsp-snippets'
" Plug 'thomasfaingnaert/vim-lsp-ultisnips'
"lspconfig
Plug 'neovim/nvim-lspconfig'
"signature help
Plug 'ray-x/lsp_signature.nvim'
"completion
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}


"completion engine and it's extensions
" Plug 'neoclide/coc.nvim', {'branch': 'release',
"             \ 'do': ':CocInstall coc-yank coc-json coc-lua coc-clangd
"             \ coc-rust-analyzer coc-vimlsp coc-snippets coc-java coc-groovy
"             \ coc-diagnostic coc-xml' }
" Plug 'rafcamlet/coc-nvim-lua'




















"{{{these integrate with vim-lsp, but suck a bit

"keep these two sets around, maybe they'll eventually become decent
"both slow af, don't match coc's fuzzy completion capabilities, and 
"also getdoc works weird
"(both need ultisnips anyways)

"snippet engine
" Plug 'SirVer/ultisnips'
" Plug 'ncm2/ncm2'
" Plug 'roxma/nvim-yarp'
" Plug 'ncm2/ncm2-vim-lsp'
" Plug 'ncm2/ncm2-ultisnips'
" Plug 'ncm2/ncm2-path'

"snippet engine
" Plug 'SirVer/ultisnips'
" Plug 'prabirshrestha/asyncomplete.vim'
" Plug 'prabirshrestha/asyncomplete-lsp.vim'
" Plug 'prabirshrestha/asyncomplete-ultisnips.vim'
" Plug 'prabirshrestha/asyncomplete-file.vim'
"}}}




call plug#end()


function! FinishBootstrapping()
    :TSInstall rust lua yaml bash json comment haskell commonlisp
    :COQdeps
endfunction

""Playground


""Vimspector config

"set base directory for configs
let g:vimspector_base_dir=expand('$HOME/.config/nvim/configs/vimspector-config/')
"TODO temporarily set default keybindings to "human" mode
" let g:vimspector_enable_mappings = 'HUMAN'
"automatically install/update gadgets
let g:vimspector_install_gadgets = [ 'CodeLLDB' ]

"ui
"set bottom bar height
let g:vimspector_bottombar_height = 8
"minimal size for code window
let g:vimspector_code_minwidth = 84
"minimal width for terminal window
let g:vimspector_terminal_minwidth = 15

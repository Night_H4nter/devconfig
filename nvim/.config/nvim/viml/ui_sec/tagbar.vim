""Bindings and settings related to tagbar provided by vista



"call vista with coc by default
let g:vista_default_executive = 'coc'


"toggle tagbar
nnoremap <Bslash>ut <cmd>Vista!!<cr>


"automatically determine scope TODO:doesn't work
" autocmd VimEnter * call vista#RunForNearestMethodOrFunction()


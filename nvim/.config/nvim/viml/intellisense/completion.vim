""Completion


"coc{{{
" let g:asyncomplete_auto_completeopt = 1
" set completeopt=menuone,noinsert,noselect,preview
" set shortmess+=c


let g:completion_matching_strategy_list = [ 'exact', 'substring', 'fuzzy' ]


" inoremap <expr> <c-k> ("\<C-n>")
" inoremap <expr> <c-l> ("\<C-p>")

" imap <expr> <c-k> pumvisible() ? ("\<C-n>") : <Plug>(asyncomplete_force_refresh)
" imap <expr> <c-l> pumvisible() ? ("\<C-p>") : <Plug>(asyncomplete_force_refresh)

" imap <c-space> <Plug>(asyncomplete_force_refresh)


" let g:UltiSnipsExpandTrigger="<tab>"
" let g:UltiSnipsJumpForwardTrigger="<tab>"
" let g:UltiSnipsJumpBackwardTrigger="<s-tab>"


" smartly trigger or navigate completion
" inoremap <expr> <c-k>
"             \ pumvisible() ? ("\<C-n>") : coc#refresh()
" inoremap <expr> <c-l>
"             \ pumvisible() ? ("\<C-p>") : coc#refresh()

" inoremap <expr> <c-k>
"             \ pumvisible() ? ("\<C-n>") : 
"             \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
"             \ <SID>check_back_space() ? "\<TAB>" :
"             \ coc#refresh()

" inoremap <expr> <c-l>
"             \ pumvisible() ? ("\<C-p>") : coc#refresh()

"navigate back and forth between snippet fields
" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? coc#_select_confirm() :
"       \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()

" function! s:check_back_space() abort
"   let col = col('.') - 1
"   return !col || getline('.')[col - 1]  =~# '\s'
" endfunction

" let g:coc_snippet_next = '<Tab>'
" let g:coc_snippet_prev = '<S-Tab>'
"}}}


"completion autostart, fzf settings and disabling the default keybindings
let g:coq_settings = { 'auto_start': 'shut-up',
            \ "keymap" : { "recommended": v:false,
            \ "bigger_preview": "<C-H>"
            \ },
            \ "match": { "exact_matches": 1,
            \ "look_ahead": 100,
            \ "fuzzy_cutoff": 0.6 } 
        \ }


"keybindings
ino <silent><expr> <Esc>   pumvisible() ? "\<C-e><Esc>" : "\<Esc>"
ino <silent><expr> <C-c>   pumvisible() ? "\<C-e><C-c>" : "\<C-c>"
ino <silent><expr> <BS>    pumvisible() ? "\<C-e><BS>"  : "\<BS>"
ino <silent><expr> <Tab>   pumvisible() ? (complete_info().selected == -1 ? "\<C-e><CR>" : "\<C-y>") : "\<Tab>"
ino <silent><expr> <C-k>   pumvisible() ? "\<C-n>" : "\<C-k>"
ino <silent><expr> <C-l>   pumvisible() ? "\<C-p>" : "\<C-l>"

""UI functions and mappings



"plugin:undotree
"TODO
"if tagbar is open, then close it and
"open undotree, then open tagbar back,
"once undotree is closed
" let b:undotree_visible = 0
" function Undotree_smarttoggle()
"     if b:undotree_visible != 1
"         if tagbar_visible == 1
"             :TagbarClose<CR>
"         endif
"         let b:undotree_visible = 1
"         :UndotreeToggle<CR>
"     else
"         let b:undotree_visible = 0
"         :UndotreeToggle<CR>
"         if tagbar_visible == 1
"             :TagbarOpen<CR>
"         endif
"     endif
" endfunction


"maximizer smart toggle
function! MaximizerSmartToggle()
    if !exists("b:maximized_exists") || b:maximized_exists == 0
        let b:maximized_exists = 1
        :MaximizerToggle!
    else
        let b:maximized_exists = 0
        :MaximizerToggle!
    endif
endfunction


"unmaximize or do nothing
function! Unmaximize()
    if exists("b:maximized_exists") && b:maximized_exists == 1
        :MaximizerToggle!
        let b:maximized_exists = 0
    endif
endfunction


"clear all additional ui elements
function! UIClear()
    :UndotreeHide                       "close undotree
    :call Unmaximize()                  "unmaximize
    :TagbarClose                        "close tagbar
    :call vimspector#Reset()            "close vimspector
    :NERDTreeClose                      "close filetree
    :cclose                             "close quickfix
    :pclose                             "close preview
endfunction







"close EVERYTHING except code windows on current pane
nnoremap <bslash>uC <cmd>silent! call UIClear()<cr>

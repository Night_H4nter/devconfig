# Neovim

This is my currently in-dev config of Neovim. Refactoring implementation 
in `nvim-lspconfig` (a.k.a. native LSP) is subpar, so `vim-lsp` is used
just for that. `coq_nvim` is used as a completion engine for completion
received from native lsp.

# Build- and runtime dependencies

### Base

* Neovim version >= 0.5
* Terminal emulator with true color support
* Nerd fonts
* Git
* GNU Make
* gcc-c++
* Curl, Wget
* Zip
* Go
* Python 3 (pynvim, venv, cython)
<!-- * NodeJS (neovim module), NPM -->
* Ripgrep
* GNU Stow

### Development

Some of these might actually not need to be installed standalone.

* C/C++: Clangd, Cppcheck
* Java: JDK11, Checkstyle
* Rust: Cargo, Rust-analyzer, Rust-clippy
* Zig: ZLS
* Shell: Shellcheck
* Lua: Lualint, Sumneko Lua Language Server
* Groovy: Npm-groovy-lint
